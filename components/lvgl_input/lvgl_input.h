#include "lvgl.h"

#define PB_TOP 14
#define PB_DOWN 12

typedef struct {
    bool up;
    bool down;
} buttons_t;

extern buttons_t button;
extern uint32_t last_key;
extern lv_indev_state_t last_state;
extern lv_indev_drv_t indev_drv;
extern lv_indev_t * emulated_indev;

void btn_process();
void buttons_read(lv_indev_drv_t * drv, lv_indev_data_t*data);
void lvgl_input_init();