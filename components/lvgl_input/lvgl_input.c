#include "lvgl_input.h"
#include "driver/gpio.h"

buttons_t button;
uint32_t last_key;
lv_indev_state_t last_state;
lv_indev_drv_t indev_drv;
lv_indev_t * emulated_indev;


/*Proses Input Button*/
void btn_process() {
    if (gpio_get_level(PB_TOP) == false) {
        button.up = true;   
    }
    if (gpio_get_level(PB_DOWN) == false) {
        button.down =true;
    }
    if (button.up == true) {
        button.up = false;
        last_key = LV_KEY_ENTER;
        last_state = LV_INDEV_STATE_PR;
        goto exit;
    } else {
        last_state = LV_INDEV_STATE_REL;
    }

    if (button.down == true) {
        button.down = false;
        last_key = LV_KEY_DOWN;
        last_state = LV_INDEV_STATE_PR;
        goto exit;
    } else {
        last_state = LV_INDEV_STATE_REL;
    }

    exit: {/*DO NOTHING*/}
}

/*Driver Input Callback*/
void buttons_read(lv_indev_drv_t * drv, lv_indev_data_t*data) {
    btn_process();
    data->key = last_key;
    data->state = last_state;
}

void lvgl_input_init() {
    esp_rom_gpio_pad_select_gpio(PB_TOP);
    gpio_set_direction(PB_TOP, GPIO_MODE_INPUT);
    gpio_set_pull_mode(PB_TOP, GPIO_PULLUP_ONLY);

    esp_rom_gpio_pad_select_gpio(PB_DOWN);
    gpio_set_direction(PB_DOWN, GPIO_MODE_INPUT);
    gpio_set_pull_mode(PB_DOWN, GPIO_PULLUP_ONLY);

    lv_indev_drv_init(&indev_drv);
    indev_drv.read_cb = buttons_read;
    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    emulated_indev = lv_indev_drv_register(&indev_drv);
    lv_group_t * g = lv_group_create();
    lv_group_set_default(g);
    lv_indev_set_group(emulated_indev, g);
}